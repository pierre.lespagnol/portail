Séance Virtualisation
=====================

Virtualisation (des systèmes d'exploitation)
--------------
  * Un système d'exploitation:
    * virtualise l'accès au processeur (processus , kernel/user)
    * virtualise les interruptions et fautes matérielles (signals)
    * virtualise la mémoire (Page Table , TLBs )
    * virtualise les entrées/sorties (driver , inode , VFS )

  mais *ne virtualise pas le processeur lui-même*

  * Virtualiser le processeur, c'est
    * le partager entre plusieurs systèmes d'exploitation
    * en isolant ces systèmes (sécurité)

  * Système hôte, systèmes invités

  * Accélération matérielle: à activer dans le BIOS (VTx)

| Un niveau de privilège en plus  | et un niveau de table des pages |
|---------------------------------|---------------------------------|
| ![](figs/hypervisor-vmroot.png) | ![](figs/memory-ept.png)        |
|                Source Anandtech |  Source Y. Chung-cs.nthu.edu.tw |   

Machine virtuelle
----------
  * Noyau + système de fichiers racine(s)
  * le tout dans un fichier image: `iso` et autres `vxd`...
  * faire le `mount -o loop` sur un iso ci dessous et comparer
  avec la machine courante

[VirtualBox](https://www.virtualbox.org/)
----------
Solution de virtualisation développée par Oracle en OpenSource.
  * Installation sous Linux: `apt-get install virtualbox`
  * Installation sous Windows: téléchargez le dernier
  [installeur](https://www.virtualbox.org/wiki/Downloads) pour Windows
  * Dans les 2 cas le pack d'extension peut être utile (Shared Folders/USB)

**Configuration dans cette salle**: dans le menu `File->Preferences` de VirtualBox,
utiliser `/local/<login>`  comme `Default Machine Folder` pour stocker les VMs
(redémarrer virtualbox pour être sûr de la prise en compte).



Installation d'une distribution (ubuntu server)
----
  * La 18.04.2 fait 834Mo, voir [downloads](https://www.ubuntu.com/download/server)
  * pour la manipulation dans cette salle TP, l'iso est déjà disponible dans `/local/iso/`
  * pour information, la version utilisée est téléchargeable en ligne de commande
```
$ wget  http://releases.ubuntu.com/18.04.2/ubuntu-18.04.2-live-server-amd64.iso
```
  * si vous téléchargez, pensez à vérifier le sha256
```
$ sha256sum /local/iso/ubuntu-18.04.2-live-server-amd64.iso
```
  * Lancer virtualbox et créer une VM (tout par défaut)
    * Nom: `ubuntu1`, Type: `Linux` Version: `Ubuntu (64-bit)`
    * Taille mémoire: `1024 Mo`
    * Disque dur: `Créer un disque dur virtuel maintenant`, taille `10,00 Gio`
    * Type de fichier de disque dur: `VDI (Image Disque VirtualBox)`
    * Stockage sur disque dur physique: `Dynamiquement alloué`
    * Emplacement du fichier et taille: laisser inchangés `ubuntu1` et `10,00Gio`

**Configuration dans cette salle**: après avoir sélectionné la VM `ubuntu1`,
 sur le bouton `Settings->General` puis onglet `Advanced`, vérifier que
 `Snapshot folder` pointe sur `/local/<login>/...` et pas `/home/../<login>/...`

  * Avant de lancer la VM, mettez presse-papier partagé et glisser/déposer à `bidirectionnel` dans General/Avancé
  * Lancer la VM `ubuntu1` (bouton `Start`) et à l'invite, monter l'iso comme
  image de démarrage à son lancement. Utiliser les touches `haut/bas/tabulation`
  pour les déplacements entre menus, ainsi que les touches `entreé/espace` pour valider.

```
langue:francais,
clavier:francais,  
layout: francais,
installer Ubuntu,
réseau : par defaut,
pas de proxy,
mirror: par defaut,
utiliser un disque entier,
system de fichiers : VBOX ... 10G,
config du systeme de fichiers : par default, **Continuer**,
proposer un login/mdp :
  name:localuser,
  servername:ubuntu1,
  username:localuser,
  password:localuser,
ne pas installer le serveur ssh,
featured server snaps:aucun,
Terminé
```
  * gestion de paquetages (`apt-cache/apt-get`)
  * une distribution maintient un ensemble de paquetages disponibles dans ses dépôts, et assure la cohérence des dépendances entre ces paquetages
  * la maintenance se fait en mode administrateur	  
```
$ sudo -i
```
     que vous devrez quitter par `exit` à la fin des installations.
  * mise à jour du cache local de paquetages
```
# apt-get update
```

  * recherche d'un paquetage et affichage d'informations
```
# apt-cache search openssh
# apt-cache search openssh | grep server
# apt-cache show  openssh-server
```

  * installation d'un paquetage (et de ses dépendances)
```
# apt-get install openssh-server
```

  * suppression d'un paquetage (et de ses dépendances)
```
# apt-get remove openssh-server
```

Exemple d'installation d'un serveur web
----
  * 1/3 : installation du paquetage d'un serveur web
```
# apt-get install nginx
```
  * 2/3: installation du paquetage d'un client web
```
# apt-get install lynx
```
  * 3/3: test du serveur
```
$ lynx localhost
$ lynx localhost:80
```
  * pour rendre visible le serveur web depuis l'hôte (en plus de la VM), il faut faire une redirection de port dans Virtualbox:
  ` Settings->Network->Advanced->Port forwarding :`
```
protocole:TCP,
Host IP:127.0.0.1,  
Host Port:8080,
Guest IP:10.0.2.15,  
Guest Port:80
```
  * puis tester dans un navigateur depuis l'hôte, en vous connectant à l'url `http://localhost:8080`

  * pour rendre visible le serveur web depuis tout hôte de la salle (en plus de la VM), il faut faire une redirection de port dans Virtualbox:
  ` Settings->Network->Advanced->Port forwarding :`
```
protocole:TCP,
Host IP:<utiliser l'@ip fournie par `ip addr`>,  
Host Port:8086,
Guest IP:10.0.2.15,  
Guest Port:80
```
  * puis tester dans un navigateur  depuis votre voisin, en lui demandanr de se  connecter à l'url `http://<votre ip addr>:8086`
